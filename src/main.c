#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>

void test1(){
    heap_init(0);
    debug_heap(stderr, HEAP_START);

    int *arr = _malloc(666 * sizeof(int));
    debug_heap(stderr, HEAP_START);

    for (size_t i = 0; i < 666; i++)
        arr[i] = (int)i;
    for (size_t i = 0; i < 666; i++)
        assert(arr[i] == (int)i);
    assert((struct block_header *)(((uint8_t *)(arr)) - offsetof(struct block_header, contents)) == HEAP_START);
    debug_heap(stderr, HEAP_START);

    _free(arr);
    debug_heap(stderr, HEAP_START);
    heap_term();
}

void test2(){
    heap_init(0);
    debug_heap(stderr, HEAP_START);

    void *arr1 = _malloc(6666);
    debug_heap(stderr, HEAP_START);

    void *arr2 = _malloc(6666);
    struct block_header *first_block_header = (struct block_header *)(((uint8_t *)(arr1)) - offsetof(struct block_header, contents));
    struct block_header *second_block_header = (struct block_header *)(((uint8_t *)(arr2)) - offsetof(struct block_header, contents));
    assert(first_block_header == HEAP_START);
    assert(second_block_header == HEAP_START + size_from_capacity(first_block_header->capacity).bytes);
    debug_heap(stderr, HEAP_START);

    _free(arr1);
    debug_heap(stderr, HEAP_START);

    _free(arr2);
    debug_heap(stderr, HEAP_START);

    arr1 = _malloc(6666);
    first_block_header = (struct block_header *)(((uint8_t *)(arr1)) - offsetof(struct block_header, contents));
    assert(first_block_header == HEAP_START);
    debug_heap(stderr, HEAP_START);

    _free(arr1);
    debug_heap(stderr, HEAP_START);
    heap_term();
}

void test3(){
    heap_init(0);
    debug_heap(stderr, HEAP_START);

    int *arr3 = _malloc(6666 * sizeof(int));
    debug_heap(stderr, HEAP_START);

    for (size_t i = 0; i < 6666; i++)
        arr3[i] = (int)i;
    for (size_t i = 0; i < 6666; i++)
        assert(arr3[i] == (int)i);
    debug_heap(stderr, HEAP_START);

    _free(arr3);
    debug_heap(stderr, HEAP_START);
    heap_term();
}

void test4(){
    heap_init(0);
    debug_heap(stderr, HEAP_START);

    void *first_block = _malloc(666);
    debug_heap(stderr, HEAP_START);

    void *first_block_header2 = (struct block_header *)(((uint8_t *)(first_block)) - offsetof(struct block_header, contents));
    assert(first_block_header2 == HEAP_START);
    void *second_block = _malloc(REGION_MIN_SIZE);
    debug_heap(stderr, HEAP_START);

    void *second_block_header2 = (struct block_header *)(((uint8_t *)(second_block)) - offsetof(struct block_header, contents));
    assert(second_block_header2 != HEAP_START + REGION_MIN_SIZE);
    _free(first_block);
    debug_heap(stderr, HEAP_START);

    _free(second_block);
    debug_heap(stderr, HEAP_START);
    heap_term();
}

int main() {
    uint16_t current_test = 0;
    uint16_t test_count = 4;

    current_test++;
    fprintf(stdout, "Test %d of %d\n", current_test, test_count);
    test1();
    fprintf(stdout, "SUCCESS\n\n");

    current_test++;
    fprintf(stdout, "Test %d of %d\n", current_test, test_count);
    test2();
    fprintf(stdout, "SUCCESS\n\n");

    current_test++;
    fprintf(stdout, "Test %d of %d\n", current_test, test_count);
    test3();
    fprintf(stdout, "SUCCESS\n\n");

    current_test++;
    fprintf(stdout, "Test %d of %d\n", current_test, test_count);
    test4();
    fprintf(stdout, "SUCCESS\n\n");

    return 0;
}
